# dbsecurity
Mybatis数据库加密插件,可定制加密的表，字段等。

# 配置 mybatis配置文件
    <plugins>
        <plugin interceptor="com.stone.db.dbsecurity.interceptor.SecurityInterceptor">
            <!--指定用的数据库类型-->
            <property name="db.type" value="mysql"/>
            <!--需要加密的表名，多个请用逗号分隔开-->
            <property name="table.name" value="as_cash_user_info,as_user_info" />
            <!--需要加密的字段名(对应实体类的表名)，多个请用逗号分隔开-->
            <property name="encrypt.column.name" value="name,phone,bankCardNumber,bankUserName"/>
            <!--需要解密的字段名(对应实体类的表名)，多个请用逗号分隔开-->
            <property name="decrypt.column.name" value="name,phone,bankCardNumber,bankUserName"/>
            <!--需实现com.stone.db.dbsecurity.BaseSecuityHandler接口，自定义加密解密算法-->
            <property name="security.hanlder.class" value="XXX"/>
        </plugin>
    </plugins>
