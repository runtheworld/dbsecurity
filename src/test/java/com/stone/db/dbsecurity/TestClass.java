package com.stone.db.dbsecurity;
import com.stone.db.dbsecurity.mapper.XccxUserInfoMapper;
import com.stone.db.dbsecurity.model.ThirdPartyUserInfo;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.Reader;
import java.util.List;

/**
 * Created by 喵 on 2018/3/27.
 */
public class TestClass {


    public static void main(String[] args) {
        SqlSession session = null;

        try {
            Reader reader = Resources.getResourceAsReader("mybatis-config.xml");
            SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
            SqlSessionFactory sqlSessionFactory = builder.build(reader);
            reader.close();
            session = sqlSessionFactory.openSession();
            XccxUserInfoMapper fileMapper = session.getMapper(XccxUserInfoMapper.class);
            List<ThirdPartyUserInfo> thirdPartyUserInfos = fileMapper.selectAll();
            System.out.println(thirdPartyUserInfos);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(session != null){
                session.close();
            }
        }

    }
}
