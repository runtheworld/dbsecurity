package com.stone.db.dbsecurity.mapper;


import com.stone.db.dbsecurity.model.ThirdPartyUserInfo;

import java.util.List;

public interface XccxUserInfoMapper {

	List<ThirdPartyUserInfo> selectAll();
}
