package com.stone.db.dbsecurity.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ThirdPartyUserInfo implements Serializable {
    private Long id;
    private String orderNumber;
    private String name;
    private String phone;
    private String nationalId;
    private String validDateRange;
    private String isMarried;
    private String highestEducation;
    private String homeAddress;
    private String income;
    private String bankCardNumber;
    private String bankPlace;
    private String companyName;
    private String workPhone;
    private String workAddress;
    private String position;
    private String firstContactRelationship;
    private String firstContactName;
    private String firstContactPhone;
    private String secondContactRelationship;
    private String secondContactName;
    private String secondContactPhone;
    private String thirdContactRelationship;
    private String thirdContactName;
    private String thirdContactPhone;
    private Date rawAddTime;
    private Date rawUpdateTime;
    private String bankProvinceId;
    private String bankProvinceName;
    private String bankCityId;
    private String bankCityName;
    private String bankDistrictId;
    private String bankDistrictName;
    private String bankId;
    private String companyAddress;
    private String homeProvinceId;
    private String homeProvinceName;
    private String homeCityId;
    private String homeCityName;
    private String homeDistrictId;
    private String homeDistrictName;
    private String bankName;
    private Long accountId;
    private Integer customerId;
    private String bankCardBindPhone; //
    private String loanPurpose; // 贷款目的
    private String housingSituation; // 住房情况
    private String liveTime; // 现居住时间
    private String type; //
    private Integer companyProvinceId; // 单位所在省id
    private String companyProvinceName; // 单位所在省名称
    private Integer companyCityId; // 单位所在市id
    private String companyCityName; // 单位所在市名称
    private Integer companyDistrictId; // 单位所在区id
    private String companyDistrictName; // 单位所在区名称
    private Integer firstContactProvinceId; // 第一联系人地址(省id)
    private String firstContactProvinceName; // 第一联系人地址(省名称)
    private Integer firstContactCityId; // 第一联系人地址（市id）
    private String firstContactCityName; // 第一联系人地址（市名称）
    private Integer firstContactDistrictId; // 第一联系人地址（区id）
    private String firstContactDistrictName; // 第一联系人地址（区名称）
    private String firstContactAddress; // 第一联系人联系地址
    private Integer cnt; //
    private BigDecimal downPayRate; //
    private String provinceId; //
    private String cityId; //
    private String districtId;
    private String area; //
    private String person; //
    private String consigneeMobile; //
    private String riskRating; //
    private Integer isNewCustomer; //
    private String registerTime; //
    private Integer orderTimes; //
    private Integer delivery; //
    private String latestProductType; //
    private String latestProduct; //
    private BigDecimal cumulativeLoanAmount; //
    private Integer overDueDays; //
    private String storeName; //
    private String storeManager; //
    private String storeAddress; //
    private String storePhone; //
    private Integer debitDay;
    private String highestDegree;
    private String storeLon;
    private String storeLat;
    private String storeBusiness;
    private String businessContacts;
    private String email;
    private String profession;
    private String college;
    private String enrollmentStatus;
    private String degree;
    private String programType;
    private String degreeNo;
    private String major;
    private String registrationDate;
    private String studentNo;
    private String graduationDate;
    private String bankUserName;

    private String storeId;
    private String employeeId;

    private String discountRate;

    private String operatorTypeId;

    private String billWithHead1;

    private String asTag;

    private String packageAmount;
    private String monthlyFee;
    private String customerType;
    private String operatorArea;
    private String operatorRegisterDate;
    private String storeLatitude;

    private String storeLongitude;

    private String storeType;
    private String networkTime;
    private String userLongitude;
    private String userLatitude;
    //商户类型
    private String orderType;
    private String operatorAreaId;

    private String operatorType;

    /*******9F新增字段begin***************/
    private String acceptId;//额度申请编号 目前快惠9F所有
    private Integer applyQuota;//申请额度
    private String sourceType;//来源
    private String workYears;//工作年限
    private String registrationTime;//注册时间
    private String workMonths;//当前工作月数
    private String contactsNum;//通讯录数
    private String graduatedSchool;//毕业学校
    private Integer education;//学历
    private String admissionTime;//入学时间
    private String graduationTime;//毕业时间
    private String schoolNature;//学校性质
    private String companyIndustry;//公司行业
    private String monthlySalary;//月薪
    private String salaryMonths;//薪资月数
    private String workPlace;//工作地点
    private String deadlines;//工作期限
    private String longitude;//经度
    private String latitude;//纬度
    private String duties;//职务
    private String jobTitle;//职称
    /*******9F新增字段end***************/

    /****3E新增字段****/
    private String basePrice; //机款
    private String amountOfAs; //给爱尚的金额
    private String sellTime; //办单时间
    /****3E新增字段 end ****/
    /*******11F新增字段begin***************/
    /**
     * 月营业收入
     */
    private String monthlyOperatingIncome;
    /**
     * 月营业利润
     */
    private String monthlyOperatingProfit;
    /**
     * 公司成立时间
     */
    private String established;
    /**
     * 点点平台进驻时间
     */
    private String ddPlatformJoinTime;
    /**
     * 近一年在平台月均订货金额
     */
    private String monthlyOrderAmount;
    /**
     * 是否是存量老客户
     */
    private Integer oldCustomer;

    /*******11F新增字段end***************/

    /**
     * 订单来源(线上线下:online，offline)
     */
    private String source;

    /****14A新增字段****/
    private String qq; //用户qq
    private String loanTotalAmount;//平台历史借款总额
    private Integer loanTotalTimes;//平台历史借款次数
    private String loanOverdueSituation;//平台逾期情况
    private String isRegularCustomer;    //是否医美老客户
    /****14新增字段 end ****/

    /****16C新增字段strat****/
    private String merchantName;
    private String commodityName;
    private String commodityType;
    private String commodityBrand;
    private String commodityVersion;
    private BigDecimal commodityPrice;
    private String userType;
    private String companyPhone;
    private String companyContactName;
    private String companyContactPhone;
    private String companyContactWorkPosition;
    private String companyProvince;
    private String companyCity;
    private String companyDistrict;
    private String houseSituation;
    private String workPosition;
    private String workProfession;
    private String workType;
    private String errorMsg;
    private String status;
    private String employeeName;
    /****16C新增字段 end ****/
    /*******18C新增字段begin***************/
    private String documentType;//证件类型：身份证，驾驶证，护照
    //    private String gender;//性别
//    private String dateBirth;//出生日期
    private String mailingAddress;//通讯地址
    private String mailingAddressPostalCode;//通讯地址邮政编码
    private String homeAddressPostalCode;//居住地址邮政编码
    //    private String sesameCredit;//芝麻信用
//    private String customerSource;//客户来源
    private String salesmanId;//业务员Id
    private String productTypes;//商品类型
    private String productModel;//商品型号
    private String productBrand;//商品品牌
    //    private BigDecimal downPayment;//首付金额
    private String imei;
    private String xydAuthFileUrl;//小雨点授权文件url
    private String hjAuthFileUrl;//惠今授权文件url
    private String identificationCardFrontUrl;//身份证正面照片url
    private String identificationCardBackUrl;//身份证反面照片url
    private String identificationCardWithheadUrl;//本人手持身份证合照url
    private String faceRecognitionUrl;//客户人脸识别照片url
    /*******18C新增字段end***************/

    private String installment;

    /****7C1新增字段 start ****/
    private BigDecimal loanAmount;
    private Integer postalcode;
    private String sesameCredit;
    private String customerSource;
    private String post;
    private Integer overDueInstallment;
    private Integer isOverDue;
    private Date sqlExecuteTime;
    private String gender;   //性别
    private String dateBirth;    //出生日期
    private BigDecimal downPayment;  //预付款
    private Integer stagingNum;   //商品分期数
    private String shippingAddress; //收货地址
    private String nation;        //民族
    /****7C1新增字段 end ****/

    /****17C新增字段 start****/
    private String commodityColor;

    private BigDecimal monthlyRepayment;

    private String workTime;

    private Integer isHaveInsurance;

    private Integer isHaveServicesCharge;

    private String phoneTimeDisplay;

    private String monthlyAvgCost;

    private Integer phoneIsRealName;

    private String bankPhone;

    private String weixin;

    private String censusRegisterAdress;

    private String censusRegisterProvinces;

    private BigDecimal monthlyRent;

    private String companyProperty;

    private String companyScale;

    private Integer commodityMemory;

    private Integer haveOtherLoan;

    private String incomeType;

    private String incomeTime;

    private String oldPhoneBrand;

    private String oldPhoneVersion;

    private String oldPhonePrice;

    private String oldPhoneTimeDisplay;

    private String isAvailableOldPhone;

    private String oldPhoneImei;

    private String employeePhone;

    private String specialMerchantName;

    private String specialMerchantPhone;

    private BigDecimal loanDownPayment;

    private String comment;
    /****17C新增字段 end****/
    /****22C新增字段 bg****/
    private String registerData;
    private String userName;
    private String workProvinceId;
    private String workCityId;
    private String workDistrictId;
    private String workHours;
    private String workHomeAddress;
    private String studentrecordCollege;
    private String studentrecordEnrollmEntStatus;
    private String studentrecordDegree;
    private String studentrecorProgramType;
    private String studentrecordDegreeNo;
    private String studentrecordMajor;
    private String studentrecordRegistrationDate;
    private String studentrecordStudentNo;
    private String studentrecordGraduationDate;
    private String studentrecordSchooltime;
    private String homePeriod;
    private String homeStatus;

    /*******23C新增字段************/
    private String orderNo;
    private String userId;
    private String authFlag;
    private Date authUpdateTime;
    private Integer appAmount;
    private Integer authAmount;
    private String reasonCheck;
    private Date authAddTime;
    private String lbsType;
    private String longitudeSeg;
    private String latitudeSeg;
    private Integer wxEvent;
    private Date wxAddTime;
    private String wxOpenId;
    private Date addTime;
    private Integer sex;
    private Date birthDate;
    private Integer age;
    private String nationalDateRange;
    private String nationalPlace;
    private String nationalAddress;
    private String maritalStatus;
    private String jobStatus;
    private String scene;
    private String mobileCheck;
    private Integer educationCheck;
    private Integer isRealName;
    private Date userAddTime;
    private String orgType;
    private String orgTime;
    private String orgAmount;
    private String orgName;
    private String orgPhone;
    private String workProvince;
    private String workCity;
    private String workCounty;
    private String homeType;
    private String homeProvince;
    private String homeCity;
    private String homeCounty;
    private String homeStreet;
    private String bizName;
    private String bizLevel;
    private String bizScore;
    private String bizType;
    private String bizProvince;
    private String bizCity;
    private String bizCounty;
    private String bizAddress;
    private String bizLng;
    private String bizLat;
    private String bankCardNo;
    private String bankReservePhone;
    private String bankCode;

    /*******23C新增字段************/
    
    private String cancelFlag;
    
    private String workDistrictName;
    
    private String workCityName;
    
    private String workProvinceName;
    
	// 时间戳 yyyy-mm-hh HH:MM:ss
	private String isAgreeSignAuthFileTime;
	
	// 勾选日志
	private String isAgreeSignAuthFileLog;
	
	// 时间戳 yyyy-mm-hh HH:MM:ss
	private String isAgreeSignAuthFileTimeZt;
			
	// 勾选日志
	private String isAgreeSignAuthFileLogZt;
	
  	// 3E新增字段
	// 套餐类型
	private String packageType;
	
	// 业务类型
	private String serviceType;
	
	private String storeNumber;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the employeeName
     */
    public String getEmployeeName() {
        return employeeName;
    }

    /**
     * @param employeeName the employeeName to set
     */
    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public String getValidDateRange() {
        return validDateRange;
    }

    public void setValidDateRange(String validDateRange) {
        this.validDateRange = validDateRange;
    }

    public String getIsMarried() {
        return isMarried;
    }

    public void setIsMarried(String isMarried) {
        this.isMarried = isMarried;
    }

    public String getHighestEducation() {
        return highestEducation;
    }

    public void setHighestEducation(String highestEducation) {
        this.highestEducation = highestEducation;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getBankCardNumber() {
        return bankCardNumber;
    }

    public void setBankCardNumber(String bankCardNumber) {
        this.bankCardNumber = bankCardNumber;
    }

    public String getBankPlace() {
        return bankPlace;
    }

    public void setBankPlace(String bankPlace) {
        this.bankPlace = bankPlace;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getFirstContactRelationship() {
        return firstContactRelationship;
    }

    public void setFirstContactRelationship(String firstContactRelationship) {
        this.firstContactRelationship = firstContactRelationship;
    }

    public String getFirstContactName() {
        return firstContactName;
    }

    public void setFirstContactName(String firstContactName) {
        this.firstContactName = firstContactName;
    }

    public String getFirstContactPhone() {
        return firstContactPhone;
    }

    public void setFirstContactPhone(String firstContactPhone) {
        this.firstContactPhone = firstContactPhone;
    }

    public String getSecondContactRelationship() {
        return secondContactRelationship;
    }

    public void setSecondContactRelationship(String secondContactRelationship) {
        this.secondContactRelationship = secondContactRelationship;
    }

    public String getSecondContactName() {
        return secondContactName;
    }

    public void setSecondContactName(String secondContactName) {
        this.secondContactName = secondContactName;
    }

    public String getSecondContactPhone() {
        return secondContactPhone;
    }

    public void setSecondContactPhone(String secondContactPhone) {
        this.secondContactPhone = secondContactPhone;
    }

    public String getThirdContactRelationship() {
        return thirdContactRelationship;
    }

    public void setThirdContactRelationship(String thirdContactRelationship) {
        this.thirdContactRelationship = thirdContactRelationship;
    }

    public String getThirdContactName() {
        return thirdContactName;
    }

    public void setThirdContactName(String thirdContactName) {
        this.thirdContactName = thirdContactName;
    }

    public String getThirdContactPhone() {
        return thirdContactPhone;
    }

    public void setThirdContactPhone(String thirdContactPhone) {
        this.thirdContactPhone = thirdContactPhone;
    }

    public Date getRawAddTime() {
        return rawAddTime;
    }

    public void setRawAddTime(Date rawAddTime) {
        this.rawAddTime = rawAddTime;
    }

    public Date getRawUpdateTime() {
        return rawUpdateTime;
    }

    public void setRawUpdateTime(Date rawUpdateTime) {
        this.rawUpdateTime = rawUpdateTime;
    }

    public String getBankProvinceId() {
        return bankProvinceId;
    }

    public void setBankProvinceId(String bankProvinceId) {
        this.bankProvinceId = bankProvinceId;
    }

    public String getBankProvinceName() {
        return bankProvinceName;
    }

    public void setBankProvinceName(String bankProvinceName) {
        this.bankProvinceName = bankProvinceName;
    }

    public String getBankCityId() {
        return bankCityId;
    }

    public void setBankCityId(String bankCityId) {
        this.bankCityId = bankCityId;
    }

    public String getBankCityName() {
        return bankCityName;
    }

    public void setBankCityName(String bankCityName) {
        this.bankCityName = bankCityName;
    }

    public String getBankDistrictId() {
        return bankDistrictId;
    }

    public void setBankDistrictId(String bankDistrictId) {
        this.bankDistrictId = bankDistrictId;
    }

    public String getBankDistrictName() {
        return bankDistrictName;
    }

    public void setBankDistrictName(String bankDistrictName) {
        this.bankDistrictName = bankDistrictName;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getHomeProvinceId() {
        return homeProvinceId;
    }

    public void setHomeProvinceId(String homeProvinceId) {
        this.homeProvinceId = homeProvinceId;
    }

    public String getHomeProvinceName() {
        return homeProvinceName;
    }

    public void setHomeProvinceName(String homeProvinceName) {
        this.homeProvinceName = homeProvinceName;
    }

    public String getHomeCityId() {
        return homeCityId;
    }

    public void setHomeCityId(String homeCityId) {
        this.homeCityId = homeCityId;
    }

    public String getHomeCityName() {
        return homeCityName;
    }

    public void setHomeCityName(String homeCityName) {
        this.homeCityName = homeCityName;
    }

    public String getHomeDistrictId() {
        return homeDistrictId;
    }

    public void setHomeDistrictId(String homeDistrictId) {
        this.homeDistrictId = homeDistrictId;
    }

    public String getHomeDistrictName() {
        return homeDistrictName;
    }

    public void setHomeDistrictName(String homeDistrictName) {
        this.homeDistrictName = homeDistrictName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getBankCardBindPhone() {
        return bankCardBindPhone;
    }

    public void setBankCardBindPhone(String bankCardBindPhone) {
        this.bankCardBindPhone = bankCardBindPhone;
    }

    public String getLoanPurpose() {
        return loanPurpose;
    }

    public void setLoanPurpose(String loanPurpose) {
        this.loanPurpose = loanPurpose;
    }

    public String getHousingSituation() {
        return housingSituation;
    }

    public void setHousingSituation(String housingSituation) {
        this.housingSituation = housingSituation;
    }

    public String getLiveTime() {
        return liveTime;
    }

    public void setLiveTime(String liveTime) {
        this.liveTime = liveTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCompanyProvinceId() {
        return companyProvinceId;
    }

    public void setCompanyProvinceId(Integer companyProvinceId) {
        this.companyProvinceId = companyProvinceId;
    }

    public String getCompanyProvinceName() {
        return companyProvinceName;
    }

    public void setCompanyProvinceName(String companyProvinceName) {
        this.companyProvinceName = companyProvinceName;
    }

    public Integer getCompanyCityId() {
        return companyCityId;
    }

    public void setCompanyCityId(Integer companyCityId) {
        this.companyCityId = companyCityId;
    }

    public String getCompanyCityName() {
        return companyCityName;
    }

    public void setCompanyCityName(String companyCityName) {
        this.companyCityName = companyCityName;
    }

    public Integer getCompanyDistrictId() {
        return companyDistrictId;
    }

    public void setCompanyDistrictId(Integer companyDistrictId) {
        this.companyDistrictId = companyDistrictId;
    }

    public String getCompanyDistrictName() {
        return companyDistrictName;
    }

    public void setCompanyDistrictName(String companyDistrictName) {
        this.companyDistrictName = companyDistrictName;
    }

    public Integer getFirstContactProvinceId() {
        return firstContactProvinceId;
    }

    public void setFirstContactProvinceId(Integer firstContactProvinceId) {
        this.firstContactProvinceId = firstContactProvinceId;
    }

    public String getFirstContactProvinceName() {
        return firstContactProvinceName;
    }

    public void setFirstContactProvinceName(String firstContactProvinceName) {
        this.firstContactProvinceName = firstContactProvinceName;
    }

    public Integer getFirstContactCityId() {
        return firstContactCityId;
    }

    public void setFirstContactCityId(Integer firstContactCityId) {
        this.firstContactCityId = firstContactCityId;
    }

    public String getFirstContactCityName() {
        return firstContactCityName;
    }

    public void setFirstContactCityName(String firstContactCityName) {
        this.firstContactCityName = firstContactCityName;
    }

    public Integer getFirstContactDistrictId() {
        return firstContactDistrictId;
    }

    public void setFirstContactDistrictId(Integer firstContactDistrictId) {
        this.firstContactDistrictId = firstContactDistrictId;
    }

    public String getFirstContactDistrictName() {
        return firstContactDistrictName;
    }

    public void setFirstContactDistrictName(String firstContactDistrictName) {
        this.firstContactDistrictName = firstContactDistrictName;
    }

    public String getFirstContactAddress() {
        return firstContactAddress;
    }

    public void setFirstContactAddress(String firstContactAddress) {
        this.firstContactAddress = firstContactAddress;
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    public BigDecimal getDownPayRate() {
        return downPayRate;
    }

    public void setDownPayRate(BigDecimal downPayRate) {
        this.downPayRate = downPayRate;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getConsigneeMobile() {
        return consigneeMobile;
    }

    public void setConsigneeMobile(String consigneeMobile) {
        this.consigneeMobile = consigneeMobile;
    }

    public String getRiskRating() {
        return riskRating;
    }

    public void setRiskRating(String riskRating) {
        this.riskRating = riskRating;
    }

    public Integer getIsNewCustomer() {
        return isNewCustomer;
    }

    public void setIsNewCustomer(Integer isNewCustomer) {
        this.isNewCustomer = isNewCustomer;
    }

    public String getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(String registerTime) {
        this.registerTime = registerTime;
    }

    public Integer getOrderTimes() {
        return orderTimes;
    }

    public void setOrderTimes(Integer orderTimes) {
        this.orderTimes = orderTimes;
    }

    public Integer getDelivery() {
        return delivery;
    }

    public void setDelivery(Integer delivery) {
        this.delivery = delivery;
    }

    public String getLatestProductType() {
        return latestProductType;
    }

    public void setLatestProductType(String latestProductType) {
        this.latestProductType = latestProductType;
    }

    public String getLatestProduct() {
        return latestProduct;
    }

    public void setLatestProduct(String latestProduct) {
        this.latestProduct = latestProduct;
    }

    public BigDecimal getCumulativeLoanAmount() {
        return cumulativeLoanAmount;
    }

    public void setCumulativeLoanAmount(BigDecimal cumulativeLoanAmount) {
        this.cumulativeLoanAmount = cumulativeLoanAmount;
    }

    public Integer getOverDueDays() {
        return overDueDays;
    }

    public void setOverDueDays(Integer overDueDays) {
        this.overDueDays = overDueDays;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreManager() {
        return storeManager;
    }

    public void setStoreManager(String storeManager) {
        this.storeManager = storeManager;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public String getStorePhone() {
        return storePhone;
    }

    public void setStorePhone(String storePhone) {
        this.storePhone = storePhone;
    }

    public Integer getDebitDay() {
        return debitDay;
    }

    public void setDebitDay(Integer debitDay) {
        this.debitDay = debitDay;
    }

    public String getHighestDegree() {
        return highestDegree;
    }

    public void setHighestDegree(String highestDegree) {
        this.highestDegree = highestDegree;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getEnrollmentStatus() {
        return enrollmentStatus;
    }

    public void setEnrollmentStatus(String enrollmentStatus) {
        this.enrollmentStatus = enrollmentStatus;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getProgramType() {
        return programType;
    }

    public void setProgramType(String programType) {
        this.programType = programType;
    }

    public String getDegreeNo() {
        return degreeNo;
    }

    public void setDegreeNo(String degreeNo) {
        this.degreeNo = degreeNo;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getStudentNo() {
        return studentNo;
    }

    public void setStudentNo(String studentNo) {
        this.studentNo = studentNo;
    }

    public String getGraduationDate() {
        return graduationDate;
    }

    public void setGraduationDate(String graduationDate) {
        this.graduationDate = graduationDate;
    }

    public String getBankUserName() {
        return bankUserName;
    }

    public void setBankUserName(String bankUserName) {
        this.bankUserName = bankUserName;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(String discountRate) {
        this.discountRate = discountRate;
    }

    public String getOperatorTypeId() {
        return operatorTypeId;
    }

    public void setOperatorTypeId(String operatorTypeId) {
        this.operatorTypeId = operatorTypeId;
    }

    public String getBillWithHead1() {
        return billWithHead1;
    }

    public void setBillWithHead1(String billWithHead1) {
        this.billWithHead1 = billWithHead1;
    }

    public String getAsTag() {
        return asTag;
    }

    public void setAsTag(String asTag) {
        this.asTag = asTag;
    }

    public String getPackageAmount() {
        return packageAmount;
    }

    public void setPackageAmount(String packageAmount) {
        this.packageAmount = packageAmount;
    }

    public String getMonthlyFee() {
        return monthlyFee;
    }

    public void setMonthlyFee(String monthlyFee) {
        this.monthlyFee = monthlyFee;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getOperatorArea() {
        return operatorArea;
    }

    public void setOperatorArea(String operatorArea) {
        this.operatorArea = operatorArea;
    }

    public String getOperatorRegisterDate() {
        return operatorRegisterDate;
    }

    public void setOperatorRegisterDate(String operatorRegisterDate) {
        this.operatorRegisterDate = operatorRegisterDate;
    }

    public String getStoreLatitude() {
        return storeLatitude;
    }

    public void setStoreLatitude(String storeLatitude) {
        this.storeLatitude = storeLatitude;
    }

    public String getStoreLongitude() {
        return storeLongitude;
    }

    public void setStoreLongitude(String storeLongitude) {
        this.storeLongitude = storeLongitude;
    }

    public String getStoreType() {
        return storeType;
    }

    public void setStoreType(String storeType) {
        this.storeType = storeType;
    }

    public String getNetworkTime() {
        return networkTime;
    }

    public void setNetworkTime(String networkTime) {
        this.networkTime = networkTime;
    }

    public String getUserLongitude() {
        return userLongitude;
    }

    public void setUserLongitude(String userLongitude) {
        this.userLongitude = userLongitude;
    }

    public String getUserLatitude() {
        return userLatitude;
    }

    public void setUserLatitude(String userLatitude) {
        this.userLatitude = userLatitude;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOperatorAreaId() {
        return operatorAreaId;
    }

    public void setOperatorAreaId(String operatorAreaId) {
        this.operatorAreaId = operatorAreaId;
    }

    public String getAcceptId() {
        return acceptId;
    }

    public void setAcceptId(String acceptId) {
        this.acceptId = acceptId;
    }

    public Integer getApplyQuota() {
        return applyQuota;
    }

    public void setApplyQuota(Integer applyQuota) {
        this.applyQuota = applyQuota;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getWorkYears() {
        return workYears;
    }

    public void setWorkYears(String workYears) {
        this.workYears = workYears;
    }

    public String getRegistrationTime() {
        return registrationTime;
    }

    public void setRegistrationTime(String registrationTime) {
        this.registrationTime = registrationTime;
    }

    public String getWorkMonths() {
        return workMonths;
    }

    public void setWorkMonths(String workMonths) {
        this.workMonths = workMonths;
    }

    public String getContactsNum() {
        return contactsNum;
    }

    public void setContactsNum(String contactsNum) {
        this.contactsNum = contactsNum;
    }

    public String getGraduatedSchool() {
        return graduatedSchool;
    }

    public void setGraduatedSchool(String graduatedSchool) {
        this.graduatedSchool = graduatedSchool;
    }

    public Integer getEducation() {
        return education;
    }

    public void setEducation(Integer education) {
        this.education = education;
    }

    public String getAdmissionTime() {
        return admissionTime;
    }

    public void setAdmissionTime(String admissionTime) {
        this.admissionTime = admissionTime;
    }

    public String getGraduationTime() {
        return graduationTime;
    }

    public void setGraduationTime(String graduationTime) {
        this.graduationTime = graduationTime;
    }

    public String getSchoolNature() {
        return schoolNature;
    }

    public void setSchoolNature(String schoolNature) {
        this.schoolNature = schoolNature;
    }

    public String getCompanyIndustry() {
        return companyIndustry;
    }

    public void setCompanyIndustry(String companyIndustry) {
        this.companyIndustry = companyIndustry;
    }

    public String getMonthlySalary() {
        return monthlySalary;
    }

    public void setMonthlySalary(String monthlySalary) {
        this.monthlySalary = monthlySalary;
    }

    public String getSalaryMonths() {
        return salaryMonths;
    }

    public void setSalaryMonths(String salaryMonths) {
        this.salaryMonths = salaryMonths;
    }

    public String getWorkPlace() {
        return workPlace;
    }

    public void setWorkPlace(String workPlace) {
        this.workPlace = workPlace;
    }

    public String getDeadlines() {
        return deadlines;
    }

    public void setDeadlines(String deadlines) {
        this.deadlines = deadlines;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getDuties() {
        return duties;
    }

    public void setDuties(String duties) {
        this.duties = duties;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getOperatorType() {
        return operatorType;
    }

    public void setOperatorType(String operatorType) {
        this.operatorType = operatorType;
    }

    public String getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(String basePrice) {
        this.basePrice = basePrice;
    }

    public String getAmountOfAs() {
        return amountOfAs;
    }

    public void setAmountOfAs(String amountOfAs) {
        this.amountOfAs = amountOfAs;
    }

    public String getMonthlyOperatingIncome() {
        return monthlyOperatingIncome;
    }

    public void setMonthlyOperatingIncome(String monthlyOperatingIncome) {
        this.monthlyOperatingIncome = monthlyOperatingIncome;
    }

    public String getMonthlyOperatingProfit() {
        return monthlyOperatingProfit;
    }

    public void setMonthlyOperatingProfit(String monthlyOperatingProfit) {
        this.monthlyOperatingProfit = monthlyOperatingProfit;
    }

    public String getEstablished() {
        return established;
    }

    public void setEstablished(String established) {
        this.established = established;
    }

    public String getDdPlatformJoinTime() {
        return ddPlatformJoinTime;
    }

    public void setDdPlatformJoinTime(String ddPlatformJoinTime) {
        this.ddPlatformJoinTime = ddPlatformJoinTime;
    }

    public String getMonthlyOrderAmount() {
        return monthlyOrderAmount;
    }

    public void setMonthlyOrderAmount(String monthlyOrderAmount) {
        this.monthlyOrderAmount = monthlyOrderAmount;
    }

    public Integer getOldCustomer() {
        return oldCustomer;
    }

    public void setOldCustomer(Integer oldCustomer) {
        this.oldCustomer = oldCustomer;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(String installment) {
        this.installment = installment;
    }

    public String getStoreLon() {
        return storeLon;
    }

    public void setStoreLon(String storeLon) {
        this.storeLon = storeLon;
    }

    public String getStoreLat() {
        return storeLat;
    }

    public void setStoreLat(String storeLat) {
        this.storeLat = storeLat;
    }

    public String getStoreBusiness() {
        return storeBusiness;
    }

    public void setStoreBusiness(String storeBusiness) {
        this.storeBusiness = storeBusiness;
    }

    public String getBusinessContacts() {
        return businessContacts;
    }

    public void setBusinessContacts(String businessContacts) {
        this.businessContacts = businessContacts;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getLoanTotalAmount() {
        return loanTotalAmount;
    }

    public void setLoanTotalAmount(String loanTotalAmount) {
        this.loanTotalAmount = loanTotalAmount;
    }

    public Integer getLoanTotalTimes() {
        return loanTotalTimes;
    }

    public void setLoanTotalTimes(Integer loanTotalTimes) {
        this.loanTotalTimes = loanTotalTimes;
    }

    public String getLoanOverdueSituation() {
        return loanOverdueSituation;
    }

    public void setLoanOverdueSituation(String loanOverdueSituation) {
        this.loanOverdueSituation = loanOverdueSituation;
    }

    public String getIsRegularCustomer() {
        return isRegularCustomer;
    }

    public void setIsRegularCustomer(String isRegularCustomer) {
        this.isRegularCustomer = isRegularCustomer;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the commodityName
     */
    public String getCommodityName() {
        return commodityName;
    }

    /**
     * @param commodityName the commodityName to set
     */
    public void setCommodityName(String commodityName) {
        this.commodityName = commodityName;
    }

    /**
     * @return the commodityType
     */
    public String getCommodityType() {
        return commodityType;
    }

    /**
     * @param commodityType the commodityType to set
     */
    public void setCommodityType(String commodityType) {
        this.commodityType = commodityType;
    }

    /**
     * @return the commodityBrand
     */
    public String getCommodityBrand() {
        return commodityBrand;
    }

    /**
     * @param commodityBrand the commodityBrand to set
     */
    public void setCommodityBrand(String commodityBrand) {
        this.commodityBrand = commodityBrand;
    }

    /**
     * @return the commodityVersion
     */
    public String getCommodityVersion() {
        return commodityVersion;
    }

    /**
     * @param commodityVersion the commodityVersion to set
     */
    public void setCommodityVersion(String commodityVersion) {
        this.commodityVersion = commodityVersion;
    }

    /**
     * @return the commodityPrice
     */
    public BigDecimal getCommodityPrice() {
        return commodityPrice;
    }

    /**
     * @param commodityPrice the commodityPrice to set
     */
    public void setCommodityPrice(BigDecimal commodityPrice) {
        this.commodityPrice = commodityPrice;
    }

    /**
     * @return the userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     * @param userType the userType to set
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

    /**
     * @return the companyPhone
     */
    public String getCompanyPhone() {
        return companyPhone;
    }

    /**
     * @param companyPhone the companyPhone to set
     */
    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    /**
     * @return the companyContactName
     */
    public String getCompanyContactName() {
        return companyContactName;
    }

    /**
     * @param companyContactName the companyContactName to set
     */
    public void setCompanyContactName(String companyContactName) {
        this.companyContactName = companyContactName;
    }

    /**
     * @return the companyContactPhone
     */
    public String getCompanyContactPhone() {
        return companyContactPhone;
    }

    /**
     * @param companyContactPhone the companyContactPhone to set
     */
    public void setCompanyContactPhone(String companyContactPhone) {
        this.companyContactPhone = companyContactPhone;
    }

    /**
     * @return the companyContactWorkPosition
     */
    public String getCompanyContactWorkPosition() {
        return companyContactWorkPosition;
    }

    /**
     * @param companyContactWorkPosition the companyContactWorkPosition to set
     */
    public void setCompanyContactWorkPosition(String companyContactWorkPosition) {
        this.companyContactWorkPosition = companyContactWorkPosition;
    }

    /**
     * @return the companyProvince
     */
    public String getCompanyProvince() {
        return companyProvince;
    }

    /**
     * @param companyProvince the companyProvince to set
     */
    public void setCompanyProvince(String companyProvince) {
        this.companyProvince = companyProvince;
    }

    /**
     * @return the companyCity
     */
    public String getCompanyCity() {
        return companyCity;
    }

    /**
     * @param companyCity the companyCity to set
     */
    public void setCompanyCity(String companyCity) {
        this.companyCity = companyCity;
    }

    /**
     * @return the companyDistrict
     */
    public String getCompanyDistrict() {
        return companyDistrict;
    }

    /**
     * @param companyDistrict the companyDistrict to set
     */
    public void setCompanyDistrict(String companyDistrict) {
        this.companyDistrict = companyDistrict;
    }

    /**
     * @return the houseSituation
     */
    public String getHouseSituation() {
        return houseSituation;
    }

    /**
     * @param houseSituation the houseSituation to set
     */
    public void setHouseSituation(String houseSituation) {
        this.houseSituation = houseSituation;
    }

    /**
     * @return the workPosition
     */
    public String getWorkPosition() {
        return workPosition;
    }

    /**
     * @param workPosition the workPosition to set
     */
    public void setWorkPosition(String workPosition) {
        this.workPosition = workPosition;
    }

    /**
     * @return the workProfession
     */
    public String getWorkProfession() {
        return workProfession;
    }

    /**
     * @param workProfession the workProfession to set
     */
    public void setWorkProfession(String workProfession) {
        this.workProfession = workProfession;
    }

    /**
     * @return the workType
     */
    public String getWorkType() {
        return workType;
    }

    /**
     * @param workType the workType to set
     */
    public void setWorkType(String workType) {
        this.workType = workType;
    }

    /**
     * @return the errorMsg
     */
    public String getErrorMsg() {
        return errorMsg;
    }

    /**
     * @param errorMsg the errorMsg to set
     */
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(String dateBirth) {
        this.dateBirth = dateBirth;
    }

    public String getMailingAddress() {
        return mailingAddress;
    }

    public void setMailingAddress(String mailingAddress) {
        this.mailingAddress = mailingAddress;
    }

    public String getMailingAddressPostalCode() {
        return mailingAddressPostalCode;
    }

    public void setMailingAddressPostalCode(String mailingAddressPostalCode) {
        this.mailingAddressPostalCode = mailingAddressPostalCode;
    }

    public String getHomeAddressPostalCode() {
        return homeAddressPostalCode;
    }

    public void setHomeAddressPostalCode(String homeAddressPostalCode) {
        this.homeAddressPostalCode = homeAddressPostalCode;
    }

    public String getSesameCredit() {
        return sesameCredit;
    }

    public void setSesameCredit(String sesameCredit) {
        this.sesameCredit = sesameCredit;
    }

    public String getCustomerSource() {
        return customerSource;
    }

    public void setCustomerSource(String customerSource) {
        this.customerSource = customerSource;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public Integer getOverDueInstallment() {
        return overDueInstallment;
    }

    public void setOverDueInstallment(Integer overDueInstallment) {
        this.overDueInstallment = overDueInstallment;
    }

    public Integer getIsOverDue() {
        return isOverDue;
    }

    public void setIsOverDue(Integer isOverDue) {
        this.isOverDue = isOverDue;
    }

    public Date getSqlExecuteTime() {
        return sqlExecuteTime;
    }

    public void setSqlExecuteTime(Date sqlExecuteTime) {
        this.sqlExecuteTime = sqlExecuteTime;
    }

    public String getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(String salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getProductTypes() {
        return productTypes;
    }


    public void setProductTypes(String productTypes) {
        this.productTypes = productTypes;
    }

    public String getProductModel() {
        return productModel;
    }

    public void setProductModel(String productModel) {
        this.productModel = productModel;
    }

    public String getProductBrand() {
        return productBrand;
    }

    public void setProductBrand(String productBrand) {
        this.productBrand = productBrand;
    }

    public BigDecimal getDownPayment() {
        return downPayment;
    }

    public void setDownPayment(BigDecimal downPayment) {
        this.downPayment = downPayment;
    }

    public Integer getStagingNum() {
        return stagingNum;
    }

    public void setStagingNum(Integer stagingNum) {
        this.stagingNum = stagingNum;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getXydAuthFileUrl() {
        return xydAuthFileUrl;
    }

    public void setXydAuthFileUrl(String xydAuthFileUrl) {
        this.xydAuthFileUrl = xydAuthFileUrl;
    }

    public String getHjAuthFileUrl() {
        return hjAuthFileUrl;
    }

    public void setHjAuthFileUrl(String hjAuthFileUrl) {
        this.hjAuthFileUrl = hjAuthFileUrl;
    }

    public String getIdentificationCardFrontUrl() {
        return identificationCardFrontUrl;
    }

    public void setIdentificationCardFrontUrl(String identificationCardFrontUrl) {
        this.identificationCardFrontUrl = identificationCardFrontUrl;
    }

    public String getIdentificationCardBackUrl() {
        return identificationCardBackUrl;
    }

    public void setIdentificationCardBackUrl(String identificationCardBackUrl) {
        this.identificationCardBackUrl = identificationCardBackUrl;
    }

    public String getIdentificationCardWithheadUrl() {
        return identificationCardWithheadUrl;
    }

    public void setIdentificationCardWithheadUrl(String identificationCardWithheadUrl) {
        this.identificationCardWithheadUrl = identificationCardWithheadUrl;
    }

    public String getFaceRecognitionUrl() {
        return faceRecognitionUrl;
    }

    public void setFaceRecognitionUrl(String faceRecognitionUrl) {
        this.faceRecognitionUrl = faceRecognitionUrl;
    }

    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(BigDecimal loanAmount) {
        this.loanAmount = loanAmount;
    }

    public Integer getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(Integer postalcode) {
        this.postalcode = postalcode;
    }

    public String getCommodityColor() {
        return commodityColor;
    }

    public void setCommodityColor(String commodityColor) {
        this.commodityColor = commodityColor;
    }

    public BigDecimal getMonthlyRepayment() {
        return monthlyRepayment;
    }

    public void setMonthlyRepayment(BigDecimal monthlyRepayment) {
        this.monthlyRepayment = monthlyRepayment;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }

    public Integer getIsHaveInsurance() {
        return isHaveInsurance;
    }

    public void setIsHaveInsurance(Integer isHaveInsurance) {
        this.isHaveInsurance = isHaveInsurance;
    }

    public Integer getIsHaveServicesCharge() {
        return isHaveServicesCharge;
    }

    public void setIsHaveServicesCharge(Integer isHaveServicesCharge) {
        this.isHaveServicesCharge = isHaveServicesCharge;
    }

    public String getPhoneTimeDisplay() {
        return phoneTimeDisplay;
    }

    public void setPhoneTimeDisplay(String phoneTimeDisplay) {
        this.phoneTimeDisplay = phoneTimeDisplay;
    }

    public String getMonthlyAvgCost() {
        return monthlyAvgCost;
    }

    public void setMonthlyAvgCost(String monthlyAvgCost) {
        this.monthlyAvgCost = monthlyAvgCost;
    }

    public Integer getPhoneIsRealName() {
        return phoneIsRealName;
    }

    public void setPhoneIsRealName(Integer phoneIsRealName) {
        this.phoneIsRealName = phoneIsRealName;
    }

    public String getBankPhone() {
        return bankPhone;
    }

    public void setBankPhone(String bankPhone) {
        this.bankPhone = bankPhone;
    }

    public String getWeixin() {
        return weixin;
    }

    public void setWeixin(String weixin) {
        this.weixin = weixin;
    }

    public String getCensusRegisterAdress() {
        return censusRegisterAdress;
    }

    public void setCensusRegisterAdress(String censusRegisterAdress) {
        this.censusRegisterAdress = censusRegisterAdress;
    }

    public String getCensusRegisterProvinces() {
        return censusRegisterProvinces;
    }

    public void setCensusRegisterProvinces(String censusRegisterProvinces) {
        this.censusRegisterProvinces = censusRegisterProvinces;
    }

    public BigDecimal getMonthlyRent() {
        return monthlyRent;
    }

    public void setMonthlyRent(BigDecimal monthlyRent) {
        this.monthlyRent = monthlyRent;
    }

    public String getCompanyProperty() {
        return companyProperty;
    }

    public void setCompanyProperty(String companyProperty) {
        this.companyProperty = companyProperty;
    }

    public String getCompanyScale() {
        return companyScale;
    }

    public void setCompanyScale(String companyScale) {
        this.companyScale = companyScale;
    }

    public Integer getCommodityMemory() {
        return commodityMemory;
    }

    public void setCommodityMemory(Integer commodityMemory) {
        this.commodityMemory = commodityMemory;
    }

    public Integer getHaveOtherLoan() {
        return haveOtherLoan;
    }

    public void setHaveOtherLoan(Integer haveOtherLoan) {
        this.haveOtherLoan = haveOtherLoan;
    }

    public String getIncomeType() {
        return incomeType;
    }

    public void setIncomeType(String incomeType) {
        this.incomeType = incomeType;
    }

    public String getIncomeTime() {
        return incomeTime;
    }

    public void setIncomeTime(String incomeTime) {
        this.incomeTime = incomeTime;
    }

    public String getOldPhoneBrand() {
        return oldPhoneBrand;
    }

    public void setOldPhoneBrand(String oldPhoneBrand) {
        this.oldPhoneBrand = oldPhoneBrand;
    }

    public String getOldPhoneVersion() {
        return oldPhoneVersion;
    }

    public void setOldPhoneVersion(String oldPhoneVersion) {
        this.oldPhoneVersion = oldPhoneVersion;
    }

    public String getOldPhonePrice() {
        return oldPhonePrice;
    }

    public void setOldPhonePrice(String oldPhonePrice) {
        this.oldPhonePrice = oldPhonePrice;
    }

    public String getOldPhoneTimeDisplay() {
        return oldPhoneTimeDisplay;
    }

    public void setOldPhoneTimeDisplay(String oldPhoneTimeDisplay) {
        this.oldPhoneTimeDisplay = oldPhoneTimeDisplay;
    }

    public String getIsAvailableOldPhone() {
        return isAvailableOldPhone;
    }

    public void setIsAvailableOldPhone(String isAvailableOldPhone) {
        this.isAvailableOldPhone = isAvailableOldPhone;
    }

    public String getOldPhoneImei() {
        return oldPhoneImei;
    }

    public void setOldPhoneImei(String oldPhoneImei) {
        this.oldPhoneImei = oldPhoneImei;
    }

    public String getEmployeePhone() {
        return employeePhone;
    }

    public void setEmployeePhone(String employeePhone) {
        this.employeePhone = employeePhone;
    }

    public String getSpecialMerchantName() {
        return specialMerchantName;
    }

    public void setSpecialMerchantName(String specialMerchantName) {
        this.specialMerchantName = specialMerchantName;
    }

    public String getSpecialMerchantPhone() {
        return specialMerchantPhone;
    }

    public void setSpecialMerchantPhone(String specialMerchantPhone) {
        this.specialMerchantPhone = specialMerchantPhone;
    }

    public BigDecimal getLoanDownPayment() {
        return loanDownPayment;
    }

    public void setLoanDownPayment(BigDecimal loanDownPayment) {
        this.loanDownPayment = loanDownPayment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRegisterData() {
        return registerData;
    }

    public void setRegisterData(String registerData) {
        this.registerData = registerData;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getWorkHours() {
        return workHours;
    }

    public void setWorkHours(String workHours) {
        this.workHours = workHours;
    }

    public String getWorkHomeAddress() {
        return workHomeAddress;
    }

    public void setWorkHomeAddress(String workHomeAddress) {
        this.workHomeAddress = workHomeAddress;
    }

    public String getStudentrecordCollege() {
        return studentrecordCollege;
    }

    public void setStudentrecordCollege(String studentrecordCollege) {
        this.studentrecordCollege = studentrecordCollege;
    }

    public String getStudentrecordEnrollmEntStatus() {
        return studentrecordEnrollmEntStatus;
    }

    public void setStudentrecordEnrollmEntStatus(String studentrecordEnrollmEntStatus) {
        this.studentrecordEnrollmEntStatus = studentrecordEnrollmEntStatus;
    }

    public String getStudentrecordDegree() {
        return studentrecordDegree;
    }

    public void setStudentrecordDegree(String studentrecordDegree) {
        this.studentrecordDegree = studentrecordDegree;
    }

    public String getStudentrecorProgramType() {
        return studentrecorProgramType;
    }

    public void setStudentrecorProgramType(String studentrecorProgramType) {
        this.studentrecorProgramType = studentrecorProgramType;
    }

    public String getStudentrecordDegreeNo() {
        return studentrecordDegreeNo;
    }

    public void setStudentrecordDegreeNo(String studentrecordDegreeNo) {
        this.studentrecordDegreeNo = studentrecordDegreeNo;
    }

    public String getStudentrecordMajor() {
        return studentrecordMajor;
    }

    public void setStudentrecordMajor(String studentrecordMajor) {
        this.studentrecordMajor = studentrecordMajor;
    }

    public String getStudentrecordRegistrationDate() {
        return studentrecordRegistrationDate;
    }

    public void setStudentrecordRegistrationDate(String studentrecordRegistrationDate) {
        this.studentrecordRegistrationDate = studentrecordRegistrationDate;
    }

    public String getStudentrecordStudentNo() {
        return studentrecordStudentNo;
    }

    public void setStudentrecordStudentNo(String studentrecordStudentNo) {
        this.studentrecordStudentNo = studentrecordStudentNo;
    }

    public String getStudentrecordGraduationDate() {
        return studentrecordGraduationDate;
    }

    public void setStudentrecordGraduationDate(String studentrecordGraduationDate) {
        this.studentrecordGraduationDate = studentrecordGraduationDate;
    }

    public String getStudentrecordSchooltime() {
        return studentrecordSchooltime;
    }

    public void setStudentrecordSchooltime(String studentrecordSchooltime) {
        this.studentrecordSchooltime = studentrecordSchooltime;
    }

    public String getHomePeriod() {
        return homePeriod;
    }

    public void setHomePeriod(String homePeriod) {
        this.homePeriod = homePeriod;
    }


    public String getHomeStatus() {
        return homeStatus;
    }

    public void setHomeStatus(String homeStatus) {
        this.homeStatus = homeStatus;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAuthFlag() {
        return authFlag;
    }

    public void setAuthFlag(String authFlag) {
        this.authFlag = authFlag;
    }

    public Date getAuthUpdateTime() {
        return authUpdateTime;
    }

    public void setAuthUpdateTime(Date authUpdateTime) {
        this.authUpdateTime = authUpdateTime;
    }

    public Integer getAppAmount() {
        return appAmount;
    }

    public void setAppAmount(Integer appAmount) {
        this.appAmount = appAmount;
    }

    public Integer getAuthAmount() {
        return authAmount;
    }

    public void setAuthAmount(Integer authAmount) {
        this.authAmount = authAmount;
    }

    public String getReasonCheck() {
        return reasonCheck;
    }

    public void setReasonCheck(String reasonCheck) {
        this.reasonCheck = reasonCheck;
    }

    public Date getAuthAddTime() {
        return authAddTime;
    }

    public void setAuthAddTime(Date authAddTime) {
        this.authAddTime = authAddTime;
    }

    public String getLbsType() {
        return lbsType;
    }

    public void setLbsType(String lbsType) {
        this.lbsType = lbsType;
    }

    public String getLongitudeSeg() {
        return longitudeSeg;
    }

    public void setLongitudeSeg(String longitudeSeg) {
        this.longitudeSeg = longitudeSeg;
    }

    public String getLatitudeSeg() {
        return latitudeSeg;
    }

    public void setLatitudeSeg(String latitudeSeg) {
        this.latitudeSeg = latitudeSeg;
    }

    public Integer getWxEvent() {
        return wxEvent;
    }

    public void setWxEvent(Integer wxEvent) {
        this.wxEvent = wxEvent;
    }

    public Date getWxAddTime() {
        return wxAddTime;
    }

    public void setWxAddTime(Date wxAddTime) {
        this.wxAddTime = wxAddTime;
    }

    public String getWxOpenId() {
        return wxOpenId;
    }

    public void setWxOpenId(String wxOpenId) {
        this.wxOpenId = wxOpenId;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getNationalDateRange() {
        return nationalDateRange;
    }

    public void setNationalDateRange(String nationalDateRange) {
        this.nationalDateRange = nationalDateRange;
    }

    public String getNationalPlace() {
        return nationalPlace;
    }

    public void setNationalPlace(String nationalPlace) {
        this.nationalPlace = nationalPlace;
    }

    public String getNationalAddress() {
        return nationalAddress;
    }

    public void setNationalAddress(String nationalAddress) {
        this.nationalAddress = nationalAddress;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }

    public String getMobileCheck() {
        return mobileCheck;
    }

    public void setMobileCheck(String mobileCheck) {
        this.mobileCheck = mobileCheck;
    }

    public Integer getEducationCheck() {
        return educationCheck;
    }

    public void setEducationCheck(Integer educationCheck) {
        this.educationCheck = educationCheck;
    }

    public Integer getIsRealName() {
        return isRealName;
    }

    public void setIsRealName(Integer isRealName) {
        this.isRealName = isRealName;
    }

    public Date getUserAddTime() {
        return userAddTime;
    }

    public void setUserAddTime(Date userAddTime) {
        this.userAddTime = userAddTime;
    }

    public String getOrgType() {
        return orgType;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }

    public String getOrgTime() {
        return orgTime;
    }

    public void setOrgTime(String orgTime) {
        this.orgTime = orgTime;
    }

    public String getOrgAmount() {
        return orgAmount;
    }

    public void setOrgAmount(String orgAmount) {
        this.orgAmount = orgAmount;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgPhone() {
        return orgPhone;
    }

    public void setOrgPhone(String orgPhone) {
        this.orgPhone = orgPhone;
    }

    public String getWorkProvince() {
        return workProvince;
    }

    public void setWorkProvince(String workProvince) {
        this.workProvince = workProvince;
    }

    public String getWorkCity() {
        return workCity;
    }

    public void setWorkCity(String workCity) {
        this.workCity = workCity;
    }

    public String getWorkCounty() {
        return workCounty;
    }

    public void setWorkCounty(String workCounty) {
        this.workCounty = workCounty;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getHomeType() {
        return homeType;
    }

    public void setHomeType(String homeType) {
        this.homeType = homeType;
    }

    public String getHomeProvince() {
        return homeProvince;
    }

    public void setHomeProvince(String homeProvince) {
        this.homeProvince = homeProvince;
    }

    public String getHomeCity() {
        return homeCity;
    }

    public void setHomeCity(String homeCity) {
        this.homeCity = homeCity;
    }

    public String getHomeCounty() {
        return homeCounty;
    }

    public void setHomeCounty(String homeCounty) {
        this.homeCounty = homeCounty;
    }

    public String getHomeStreet() {
        return homeStreet;
    }

    public void setHomeStreet(String homeStreet) {
        this.homeStreet = homeStreet;
    }

    public String getBizName() {
        return bizName;
    }

    public void setBizName(String bizName) {
        this.bizName = bizName;
    }

    public String getBizLevel() {
        return bizLevel;
    }

    public void setBizLevel(String bizLevel) {
        this.bizLevel = bizLevel;
    }

    public String getBizScore() {
        return bizScore;
    }

    public void setBizScore(String bizScore) {
        this.bizScore = bizScore;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getBizProvince() {
        return bizProvince;
    }

    public void setBizProvince(String bizProvince) {
        this.bizProvince = bizProvince;
    }

    public String getBizCity() {
        return bizCity;
    }

    public void setBizCity(String bizCity) {
        this.bizCity = bizCity;
    }

    public String getBizCounty() {
        return bizCounty;
    }

    public void setBizCounty(String bizCounty) {
        this.bizCounty = bizCounty;
    }

    public String getBizAddress() {
        return bizAddress;
    }

    public void setBizAddress(String bizAddress) {
        this.bizAddress = bizAddress;
    }

    public String getBizLng() {
        return bizLng;
    }

    public void setBizLng(String bizLng) {
        this.bizLng = bizLng;
    }

    public String getBizLat() {
        return bizLat;
    }

    public void setBizLat(String bizLat) {
        this.bizLat = bizLat;
    }

    public String getBankCardNo() {
        return bankCardNo;
    }

    public void setBankCardNo(String bankCardNo) {
        this.bankCardNo = bankCardNo;
    }

    public String getBankReservePhone() {
        return bankReservePhone;
    }

    public void setBankReservePhone(String bankReservePhone) {
        this.bankReservePhone = bankReservePhone;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getSellTime() {
        return sellTime;
    }

    public void setSellTime(String sellTime) {
        this.sellTime = sellTime;
    }

    public String getWorkProvinceId() {
        return workProvinceId;
    }

    public void setWorkProvinceId(String workProvinceId) {
        this.workProvinceId = workProvinceId;
    }

    public String getWorkCityId() {
        return workCityId;
    }

    public void setWorkCityId(String workCityId) {
        this.workCityId = workCityId;
    }

    public String getWorkDistrictId() {
        return workDistrictId;
    }

    public void setWorkDistrictId(String workDistrictId) {
        this.workDistrictId = workDistrictId;
    }
    
    public String getCancelFlag() {
		return cancelFlag;
	}

	public void setCancelFlag(String cancelFlag) {
		this.cancelFlag = cancelFlag;
	}

	public String getWorkDistrictName() {
		return workDistrictName;
	}

	public void setWorkDistrictName(String workDistrictName) {
		this.workDistrictName = workDistrictName;
	}

	public String getWorkCityName() {
		return workCityName;
	}

	public void setWorkCityName(String workCityName) {
		this.workCityName = workCityName;
	}

	public String getWorkProvinceName() {
		return workProvinceName;
	}

	public void setWorkProvinceName(String workProvinceName) {
		this.workProvinceName = workProvinceName;
	}

    public String getIsAgreeSignAuthFileTime() {
        return isAgreeSignAuthFileTime;
    }

    public void setIsAgreeSignAuthFileTime(String isAgreeSignAuthFileTime) {
        this.isAgreeSignAuthFileTime = isAgreeSignAuthFileTime;
    }

    public String getIsAgreeSignAuthFileLog() {
        return isAgreeSignAuthFileLog;
    }

    public void setIsAgreeSignAuthFileLog(String isAgreeSignAuthFileLog) {
        this.isAgreeSignAuthFileLog = isAgreeSignAuthFileLog;
    }
    
    public String getIsAgreeSignAuthFileTimeZt() {
		return isAgreeSignAuthFileTimeZt;
	}

	public void setIsAgreeSignAuthFileTimeZt(String isAgreeSignAuthFileTimeZt) {
		this.isAgreeSignAuthFileTimeZt = isAgreeSignAuthFileTimeZt;
	}

	public String getIsAgreeSignAuthFileLogZt() {
		return isAgreeSignAuthFileLogZt;
	}

	public void setIsAgreeSignAuthFileLogZt(String isAgreeSignAuthFileLogZt) {
		this.isAgreeSignAuthFileLogZt = isAgreeSignAuthFileLogZt;
	}

	public String getPackageType() {
		return packageType;
	}

	public void setPackageType(String packageType) {
		this.packageType = packageType;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	
	public String getStoreNumber() {
		return storeNumber;
	}

	public void setStoreNumber( String storeNumber ) {
		this.storeNumber = storeNumber;
	}


}