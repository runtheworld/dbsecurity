package com.stone.db.dbsecurity;

/**
 * Created by stone on 2017/11/30.
 */
public interface BaseSecurityHandler {

	/**
	 * 加密
	 * @param parameter
	 * @return
	 */
	String encrypt(String parameter);

	/**
	 * 解密
	 * @param parameter
	 * @return
	 */
	String decrypt(String parameter);
}
