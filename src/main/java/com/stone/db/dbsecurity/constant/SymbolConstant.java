package com.stone.db.dbsecurity.constant;

/**
 * Created by 喵 on 2018/3/27.
 */
public final class SymbolConstant {

    public static final String TABLENAME = "table.name";
    public static final String ENCRYPT_COLUMN_NAME = "encrypt.column.name";
    public static final String DECRYPT_COLUMN_NAME = "decrypt.column.name";
    public static final String SECURITY_HANDLER_CLASS = "security.handler.class";
    public static final String DB_TYPE = "db.type";
    public static final String TABLE_COLUMN_ENCRYPT= "table.column.encrypt";
    public static final String TABLE_COLUMN_DECRYPT= "table.column.decrypt";



    public static final String SEPARATOR= ",";
    public static final String EQUAL = "=";
    public static final String SEPARATOR_FEN = ";";
}
