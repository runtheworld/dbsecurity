package com.stone.db.dbsecurity.uitl;

import com.stone.db.dbsecurity.constant.SymbolConstant;
import com.stone.db.dbsecurity.exception.TableColumnException;

import java.util.*;

/**
 * Created by 喵 on 2018/3/27.
 */
public final class PropertiesUtil {


    /**
     *
     * @param string
     * @param type
     * @return
     */
    public static Map<String, List<String>> parseTableColumn(String string, String type, Set<String> tableNames) {
        Map<String, List<String>> map = new HashMap<>(10);
        if (StringUtil.isEmpty(string)) {
            return null;
        }
        String[] array = string.split(SymbolConstant.SEPARATOR_FEN);
        for (int i = 0; i < array.length; i++) {
            String str = array[i];
            String[] arr = str.split(SymbolConstant.EQUAL);
            if (arr.length != 2) {
                throw new TableColumnException("[ type : " + str + " 属性设置出错]");
            }
            //获取到表名
            String tableName = arr[0];
            String columnStr = arr[1];
            String[] columnArr = columnStr.split(SymbolConstant.SEPARATOR);
            //获取到列
            List<String> colums = Arrays.asList(columnArr);
            map.put(tableName, colums);
            tableNames.add(tableName);
        }
        return map;
    }
}
