package com.stone.db.dbsecurity;

import java.util.Map;

/**
 * Created by stone on 2017/11/29.
 */
public  final  class SQLParseResult {

	private String tableName;

	private String sql;

	private Map<String,Object> columns;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public Map<String, Object> getColumns() {
		return columns;
	}

	public void setColumns(Map<String, Object> columns) {
		this.columns = columns;
	}



}
